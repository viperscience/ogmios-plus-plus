// Copyright (c) 2024 Viper Science LLC
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#ifndef _OGMIOS_CLIENT_HPP_
#define _OGMIOS_CLIENT_HPP_

// Standard Library Headers
#include <functional>
#include <future>
#include <memory>
#include <random>
#include <span>
#include <string>
#include <unordered_map>
#include <vector>

// Third-Party Headers
#include <ixwebsocket/IXWebSocket.h>

#include <nlohmann/json.hpp>

using json = nlohmann::json;

/// @brief Top-level namespace for all Ogmios objects.
namespace ogmios
{

/// @brief The Ogmios Client.
class Client
{
  private:
    // The socket object that handles the ogmios server connection.
    ix::WebSocket socket_;

    // The callback function to send logging messages to. This allows the user
    // of the class to select how to log messages are handled.
    std::function<void(int, const std::string&)> loggerCallback_{};

    // A map of promises that are waiting for a response from the ogmios server.
    // The key is the request ID and the value is the promise object that is
    // waiting for the response. The map is used to store the promises so that
    // they can be resolved when the response is received.
    std::unordered_map<std::string, std::shared_ptr<std::promise<json>>>
        promises_;

    // Use a mutex to protect the promises map from concurrent access. This is
    // required because the promises map is accessed by the websocket callbacks
    // and the websocket callbacks are called from the websocket thread. This
    // could lead to a race condition if the promises map is accessed from
    // multiple threads. The mutex ensures that the promises map is only
    // accessed from one thread at a time.
    mutable std::mutex mtx_;

    // A random number generator to generate request IDs.
    std::mt19937 rng_;

    // Used to store potential socket errors encountered in the response
    // callback.
    std::string error_msg_;

    // A flag to indicate if the client is connected to the ogmios server.
    bool is_connected_;

    // Internal function for processing log messages.
    // @param level The log level.
    // @param msg The log message.
    auto log(int level, const std::string& msg) const -> void;

  public:
    /// @brief Create a new Ogmios Client using the default URL.
    /// @note The default URL is "ws://localhost:1337".
    Client() : Client("ws://localhost:1337"){};

    /// @brief Create a new Ogmios Client.
    /// @param url The URL of the Ogmios server.
    /// @param timeout_ms Time to wait for the connection to open.
    Client(const std::string& url, uint32_t timeout_ms = 1000);

    /// @brief Destroy the Ogmios Client.
    /// @note This will close the connection to the Ogmios server.
    ~Client();

    Client(const Client&) = delete;             // non construction-copyable
    Client& operator=(const Client&) = delete;  // non copyable

    /// @brief Set the logger callback.
    /// @param callback The callback function.
    auto setLoggerCallback(std::function<void(int, const std::string&)> callback
    ) -> void
    {
        this->loggerCallback_ = callback;
    }  // setLoggerCallback

    /// @brief Send the JSON-RPC message to the Ogmios server via websocket.
    /// @param msg The JSON message to send.
    /// @return The future result as a std::future<json> object.
    auto rpcSend(json msg) -> std::future<json>;

    ////////////////////////////////////////////////////////////////////////////
    // Chain Synchronization Mini-Protocol
    ////////////////////////////////////////////////////////////////////////////

    /// @brief Set the intersection of the client and node at the origin.
    /// @return The future result as a std::future<json> object.
    auto findIntersectionOrigin() -> std::future<json>;

    /// @brief Set the intersection of the client and node at the given point.
    /// @param point The point to set the intersection to.
    /// @return The future result as a std::future<json> object.
    auto findIntersection(const json& points) -> std::future<json>;

    /// @brief Ask the node for the next block from the current cusor position.
    /// @return The future result as a std::future<json> object.
    auto nextBlock() -> std::future<json>;

    ////////////////////////////////////////////////////////////////////////////
    // Ledger State Mini-Protocol
    ////////////////////////////////////////////////////////////////////////////

    /// @brief Aquire the ledger state at the given point.
    /// @note The ledger state must be aquired before querying ledger info. If a
    ///       specific ledger state is not acquired, ledger state queries will
    ///       be made against the current tip state.
    /// @param point The point to get the ledger state at.
    /// @return The future result as a std::future<json> object.
    auto acquireLedgerState(const json& point) -> std::future<json>;

    /// @brief Query the chain’s highest block number.
    /// @return The query result as a std::future<json> object.
    auto queryBlockHeight() -> std::future<json>;

    /// @brief Query the current epoch of the ledger.
    /// @return The query result as a std::future<json> object.
    auto queryEpoch() -> std::future<json>;

    /// @brief Query information regarding the beginning of the current ledger
    /// era.
    /// @return The query result as a std::future<json> object.
    auto queryEraStart() -> std::future<json>;

    /// @brief Query era bounds and slot parameters details, required for proper
    /// slotting arithmetic.
    /// @return The query result as a std::future<json> object.
    auto queryEraSummaries() -> std::future<json>;

    /// @brief Query the genesis configuration of a specific era.
    /// @param era The era to query the genesis configuration for. Valid options
    /// are (currently): "byron", "shelley", "alonzo", and "conway".
    /// @return The query result as a std::future<json> object.
    auto queryGenesisConfiguration(const std::string& era) -> std::future<json>;

    /// @brief Query the ledger tip.
    /// @return The query result as a std::future<json> object.
    auto queryLedgerTip() -> std::future<json>;

    /// @brief Query theistribution of the stake across all known stake pools,
    /// relative to the total stake in the network.
    /// @return The query result as a std::future<json> object.
    auto queryLiveStakeDistribution() -> std::future<json>;

    /// @brief Query the network’s current tip.
    /// @return The query result as a std::future<json> object.
    auto queryNetworkTip() -> std::future<json>;

    /// @brief Query the projected rewards of an account in a context where the
    /// top stake pools are fully saturated.
    /// @note This projection gives, in principle, a ranking of stake pools that
    /// maximizes delegator rewards.
    /// @param params The pre-constructed query parameters as json object.
    /// @return The query result as a std::future<json> object.
    auto queryProjectedRewards(const json& params) -> std::future<json>;

    /// @brief Query the projected rewards of an account in a context where the
    /// top stake pools are fully saturated.
    /// @note This projection gives, in principle, a ranking of stake pools that
    /// maximizes delegator rewards.
    /// @param stake_balances The stake balances to rank the pools for.
    /// @param stake_keys The stake keys to rank the pools for.
    /// @param stake_scripts The stake scripts to rank the pools for.
    /// @return The query result as a std::future<json> object.
    auto queryProjectedRewards(
        std::span<const size_t> stake_balances,
        const std::vector<std::string>& stake_keys,
        const std::vector<std::string>& stake_scripts
    ) -> std::future<json>;

    /// @brief Query the last update proposal w.r.t. protocol parameters, if
    /// any.
    /// @return The query result as a std::future<json> object.
    auto queryProposedProtocolParameters() -> std::future<json>;

    /// @brief Query the current protocol parameters.
    /// @return The query result as a std::future<json> object.
    auto queryProtocolParameters() -> std::future<json>;

    /// @brief Query the current delegation settings and rewards of chosen
    /// reward accounts.
    /// @param params The pre-constructed query parameters as json object.
    /// @return The query result as a std::future<json> object.
    auto queryRewardAccountSummaries(const json& params) -> std::future<json>;

    /// @brief Query details about rewards calculation for the ongoing epoch.
    /// @return The query result as a std::future<json> object.
    auto queryRewardsProvenance() -> std::future<json>;

    /// @brief Query the list of all currently registered and active stake pools
    /// with their current parameters.
    /// @param ids The list of pool IDs to query. If empty, all pools are
    /// queried.
    /// @return The query result as a std::future<json> object.
    auto queryStakePools(const std::vector<std::string>& ids = {})
        -> std::future<json>;

    /// @brief Query the chain’s start time (UTC).
    /// @return The query result as a std::future<json> object.
    auto queryStartTime() -> std::future<json>;

    /// @brief Query the current UTXO, possibly filtered by output reference.
    /// @param params The pre-constructed query parameters as json object.
    /// @return The query result as a std::future<json> object.
    auto queryUtxo(const json& params = {}) -> std::future<json>;

    ////////////////////////////////////////////////////////////////////////////
    // Mempool Monitoring Mini-Protocol
    ////////////////////////////////////////////////////////////////////////////

    /// @brief Acquire a mempool snapshot for running queries.
    /// @note The mempool mini-protocol is stateful and a memempool snapshot is
    /// required prior to calling the other mempool methods.
    /// @return The result information as a std::future<json> object.
    auto acquireMempool() -> std::future<json>;

    /// @brief Release a previously acquired a mempool snapshot.
    /// @return The result information as a std::future<json> object.
    auto releaseMempool() -> std::future<json>;

    /// @brief Return size information about the mempool.
    /// @note Requires a previouly aquired mempool snapshot.
    /// @return The result as a std::future<json> object.
    auto sizeOfMempool() -> std::future<json>;

    /// @brief Query for the next transaction in the mempool.
    /// @note Use this repeatedly to list all transactions from the mempool.
    /// @note Requires a previouly aquired mempool snapshot.
    /// @return The result as a std::future<json> object.
    auto nextTransaction(bool full = false) -> std::future<json>;

    /// @brief Query for a specific transaction in the mempool.
    /// @note Requires a previouly aquired mempool snapshot.
    /// @return The result as a std::future<json> object.
    auto hasTransaction(std::string_view txid) -> std::future<json>;

    ////////////////////////////////////////////////////////////////////////////
    // Transaction Submission Mini-Protocol
    ////////////////////////////////////////////////////////////////////////////

    /// @brief Submit a transaction to the node.
    /// @param params The JSON parameters for the websocket method.
    /// @return The result as a std::future<json> object.
    auto submitTransaction(const json& params) -> std::future<json>;

    /// @brief Submit a transaction to the node.
    /// @param cbor The transaction serialized to a CBOR base-16 string.
    /// @return The result as a std::future<json> object.
    auto submitTransactionCbor(const std::string& cbor) -> std::future<json>;

    /// @brief Evaluate script execution units without submitting the tx.
    /// @param params The JSON parameters for the websocket method.
    /// @return The result as a std::future<json> object.
    auto evaluateTransaction(const json& params) -> std::future<json>;

    /// @brief Evaluate script execution units without submitting the tx.
    /// @param cbor The transaction serialized to a CBOR base-16 string.
    /// @return The result as a std::future<json> object.
    auto evaluateTransactionCbor(std::string_view cbor) -> std::future<json>;
};

}  // namespace ogmios

#endif
