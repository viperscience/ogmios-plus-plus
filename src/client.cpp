// Copyright (c) 2024 Viper Science LLC
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include <ogmios/client.hpp>

// Standard Library Headers
#include <algorithm>
#include <array>
#include <chrono>
#include <random>
#include <thread>

// Third-Party Headers
#include "uuid.hpp"

using namespace std::chrono_literals;
using namespace nlohmann::literals;

namespace
{

// Create an enum that can alias to an int.
enum class LogMessageType : int
{
    TRACE = 0,
    DEBUG = 1,
    INFO = 2,
    WARN = 3,
    ERROR = 4,
    CRITICAL = 5,
    OFF = 6,
};

constexpr auto DEBUG = static_cast<int>(LogMessageType::DEBUG);
constexpr auto ERROR = static_cast<int>(LogMessageType::ERROR);

}  // unnamed namespace

ogmios::Client::Client(const std::string& url, uint32_t timeout_ms)
{
    // Add a default callback method.
    socket_.setOnMessageCallback(
        [&](const ix::WebSocketMessagePtr& msg)
        {
            if (msg->type == ix::WebSocketMessageType::Message)
            {
                this->log(DEBUG, "Received message: " + msg->str);
                auto msg_json = json::parse(msg->str);
                if (msg_json.contains("id"))
                {
                    const auto lock = std::lock_guard<std::mutex>(this->mtx_);
                    auto msg_id = msg_json["id"].get<std::string>();
                    if (promises_.contains(msg_id))
                    {
                        promises_[msg_id]->set_value(msg_json);
                        promises_.erase(msg_id);
                    }
                }
            }
            else if (msg->type == ix::WebSocketMessageType::Open)
            {
                this->log(DEBUG, "Connection established.");
                this->is_connected_ = true;
            }
            else if (msg->type == ix::WebSocketMessageType::Close)
            {
                this->log(DEBUG, "Connection closed.");
                this->is_connected_ = false;
            }
            else if (msg->type == ix::WebSocketMessageType::Error)
            {
                this->error_msg_ = "Connection error: " + msg->errorInfo.reason;
                this->log(ERROR, this->error_msg_);
            }
        }
    );

    this->log(DEBUG, "Connecting to " + socket_.getUrl());
    socket_.setUrl(url);
    socket_.start();

    // Wait for the connection to be established.
    auto max_wait_time = std::chrono::milliseconds(timeout_ms);
    auto total_wait_time = std::chrono::milliseconds(0ms);
    while ((socket_.getReadyState() != ix::ReadyState::Open) &&
           (total_wait_time <= max_wait_time))
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(100ms));
        total_wait_time += 100ms;
    }

    if (socket_.getReadyState() != ix::ReadyState::Open)
    {
        this->log(ERROR, "Connection timed out.");
        throw std::runtime_error("Failed to open the websocket connection.");
    }
    else
    {
        this->log(DEBUG, "Successfully opened the websocket connection.");
    }

    // Create a psudo-random number generator with a timestamp seed. This is
    // only for correlating requests and responses so it doesn't need to meet
    // any security standards for randomness.
    auto seed1 = std::chrono::system_clock::now().time_since_epoch().count();
    this->rng_ = std::mt19937(static_cast<unsigned int>(seed1));
}  // ogmios::Client::Client

ogmios::Client::~Client()
{
    this->log(DEBUG, "Closing connection to " + socket_.getUrl());
    socket_.stop();
}  // ogmios::Client::~Client

auto ogmios::Client::log(int level, const std::string& msg) const -> void
{
    if (this->loggerCallback_) this->loggerCallback_(level, msg);
}  // ogmios::Client::log

auto ogmios::Client::rpcSend(json msg) -> std::future<json>
{
    // Use the promise/future idiom for handling the websocket async response.
    // Wrap the promise in a shared pointer that can be stored in the client.
    auto promise_ptr = std::make_shared<std::promise<json>>();
    auto future_response = promise_ptr->get_future();

    // Generate a random UUID string for the message ID. This is used to
    // correlate requests and responses.
    auto uuid_gen = uuids::uuid_random_generator{this->rng_};
    const auto uuid_string = uuids::to_string(uuid_gen());

    // Use a lock around updating the promises map.
    {
        const auto lock = std::lock_guard<std::mutex>(this->mtx_);
        this->promises_[uuid_string] = promise_ptr;
    }

    // Finish building the JSON message.
    msg["jsonrpc"] = "2.0";
    msg["id"] = uuid_string;
    const auto msg_str = msg.dump();

    // Send the JSON message over the websocket connection.
    this->log(DEBUG, "Sending message: " + msg_str);
    this->socket_.send(msg_str);

    return future_response;
}  // ogmios::Client::rpcSend

////////////////////////////////////////////////////////////////////////////////
// Chain Synchronization Mini-Protocol
////////////////////////////////////////////////////////////////////////////////

auto ogmios::Client::findIntersectionOrigin() -> std::future<json>
{
    auto msg = json::parse(R"(
        {
            "method": "findIntersection",
            "params": {
                "points": [
                    "origin"
                ]
            }
        }
    )");
    return this->rpcSend(msg);
}  // findIntersectionOrigin

auto ogmios::Client::findIntersection(const json& points) -> std::future<json>
{
    auto msg = json{{"method", "findIntersection"}};
    msg["params"]["points"] = points;
    return this->rpcSend(msg);
}  // ogmios::Client::findIntersection

auto ogmios::Client::nextBlock() -> std::future<json>
{
    auto msg = json{{"method", "nextBlock"}};
    return this->rpcSend(msg);
}  // ogmios::Client::nextBlock

////////////////////////////////////////////////////////////////////////////////
// Ledger State Mini-Protocol
////////////////////////////////////////////////////////////////////////////////

auto ogmios::Client::acquireLedgerState(const json& point) -> std::future<json>
{
    auto msg = json{{"method", "acquireLedgerState"}};
    msg["params"]["point"] = point;
    return this->rpcSend(msg);
}  // ogmios::Client::acquireLedgerState

auto ogmios::Client::queryBlockHeight() -> std::future<json>
{
    auto msg = json{{"method", "queryNetwork/blockHeight"}};
    return this->rpcSend(msg);
}  // ogmios::Client::blockHeight

auto ogmios::Client::queryEpoch() -> std::future<json>
{
    auto msg = json{{"method", "queryLedgerState/epoch"}};
    return this->rpcSend(msg);
}  // ogmios::Client::queryEpoch

auto ogmios::Client::queryEraStart() -> std::future<json>
{
    auto msg = json{{"method", "queryLedgerState/eraStart"}};
    return this->rpcSend(msg);
}  // ogmios::Client::queryEraStart

auto ogmios::Client::queryEraSummaries() -> std::future<json>
{
    auto msg = json{{"method", "queryLedgerState/eraSummaries"}};
    return this->rpcSend(msg);
}  // ogmios::Client::queryEraSummaries

auto ogmios::Client::queryGenesisConfiguration(const std::string& era)
    -> std::future<json>
{
    constexpr auto era_options =
        std::array<std::string_view, 4>{"byron", "shelley", "alonzo", "conway"};
    auto it = std::find(std::begin(era_options), std::end(era_options), era);
    if (it == std::end(era_options))
    {
        throw std::runtime_error("Invalid era.");
    }
    auto msg = json{{"method", "queryNetwork/genesisConfiguration"}};
    msg["params"]["era"] = era;
    return this->rpcSend(msg);
}  // ogmios::Client::queryGenesisConfiguration

auto ogmios::Client::queryLedgerTip() -> std::future<json>
{
    auto msg = json{{"method", "queryLedgerState/tip"}};
    return this->rpcSend(msg);
}  // ogmios::Client::queryLedgerTip

auto ogmios::Client::queryLiveStakeDistribution() -> std::future<json>
{
    auto msg = json{{"method", "queryLedgerState/liveStakeDistribution"}};
    return this->rpcSend(msg);
}  // ogmios::Client::queryLiveStakeDistribution

auto ogmios::Client::queryNetworkTip() -> std::future<json>
{
    auto msg = json{{"method", "queryNetwork/tip"}};
    return this->rpcSend(msg);
}  // ogmios::Client::queryNetworkTip

auto ogmios::Client::queryProjectedRewards(const json& params)
    -> std::future<json>
{
    auto msg = json{{"method", "queryLedgerState/projectedRewards"}};
    msg["params"] = params;
    return this->rpcSend(msg);
}  // ogmios::Client::queryProjectedRewards

auto ogmios::Client::queryProjectedRewards(
    std::span<const size_t> stake_balances,
    const std::vector<std::string>& stake_keys,
    const std::vector<std::string>& stake_scripts
) -> std::future<json>
{
    if ((stake_balances.size() + stake_keys.size() + stake_scripts.size()) == 0)
    {
        throw std::runtime_error("Invalid number of inputs.");
    }

    auto params = "{}"_json;
    if (stake_balances.size() > 0)
    {
        params["stake"] = "[]"_json;
        for (size_t i = 0; i < stake_balances.size(); i++)
        {
            params["stake"][i] = R"({"ada": {"lovelace": 1000000}})"_json;
            params["stake"][i]["ada"]["lovelace"] = stake_balances[i];
        }
    }
    if (stake_keys.size() > 0)
    {
        params["keys"] = "[]"_json;
        for (size_t i = 0; i < stake_keys.size(); i++)
        {
            params["keys"][i] = stake_keys[i];
        }
    }
    if (stake_scripts.size() > 0)
    {
        params["scripts"] = "[]"_json;
        for (size_t i = 0; i < stake_scripts.size(); i++)
        {
            params["stake_scripts"][i] = stake_scripts[i];
        }
    }

    return this->queryProjectedRewards(params);
}  // ogmios::Client::queryProjectedRewards

auto ogmios::Client::queryProposedProtocolParameters() -> std::future<json>
{
    auto msg = json{{"method", "queryLedgerState/proposedProtocolParameters"}};
    return this->rpcSend(msg);
}  // ogmios::Client::queryProposedProtocolParameters

auto ogmios::Client::queryProtocolParameters() -> std::future<json>
{
    auto msg = json{{"method", "queryLedgerState/protocolParameters"}};
    return this->rpcSend(msg);
}  // ogmios::Client::queryProtocolParameters

auto ogmios::Client::queryRewardAccountSummaries(const json& params)
    -> std::future<json>
{
    auto msg = json{{"method", "queryLedgerState/rewardAccountSummaries"}};
    msg["params"] = params;
    return this->rpcSend(msg);
}  // ogmios::Client::queryRewardAccountSummaries

auto ogmios::Client::queryRewardsProvenance() -> std::future<json>
{
    auto msg = json{{"method", "queryLedgerState/rewardsProvenance"}};
    return this->rpcSend(msg);
}  // ogmios::Client::queryRewardsProvenance

auto ogmios::Client::queryStakePools(const std::vector<std::string>& ids)
    -> std::future<json>
{
    auto msg = json{{"method", "queryLedgerState/stakePools"}};
    if (!ids.empty())
    {
        msg["params"]["stakePools"] = "[]"_json;
        for (size_t i = 0; i < ids.size(); i++)
        {
            msg["params"]["stakePools"][i] = json({{"id", ids[i]}});
        }
    }
    return this->rpcSend(msg);
}  // ogmios::Client::queryStakePools

auto ogmios::Client::queryStartTime() -> std::future<json>
{
    auto msg = json{{"method", "queryNetwork/startTime"}};
    return this->rpcSend(msg);
}  // ogmios::Client::queryStartTime

auto ogmios::Client::queryUtxo(const json& params) -> std::future<json>
{
    auto msg = json{{"method", "queryLedgerState/utxo"}};
    if (!params.empty())
    {
        msg["params"] = params;
    }
    return this->rpcSend(msg);
}  // ogmios::Client::queryUtxo

////////////////////////////////////////////////////////////////////////////////
// Mempool Mini-Protocol
////////////////////////////////////////////////////////////////////////////////

auto ogmios::Client::acquireMempool() -> std::future<json>
{
    auto msg = json{{"method", "acquireMempool"}};
    return this->rpcSend(msg);
}  // ogmios::Client::acquireMempool

auto ogmios::Client::releaseMempool() -> std::future<json>
{
    auto msg = json{{"method", "releaseMempool"}};
    return this->rpcSend(msg);
}  // ogmios::Client::releaseMempool

auto ogmios::Client::sizeOfMempool() -> std::future<json>
{
    auto msg = json{{"method", "sizeOfMempool"}};
    return this->rpcSend(msg);
}  // ogmios::Client::sizeOfMempool

auto ogmios::Client::nextTransaction(bool full) -> std::future<json>
{
    auto msg = json::parse(R"(
        {
            "method": "nextTransaction",
            "params": {}
        }
    )");
    if (full)
    {
        msg["params"]["fields"] = "all";
    }
    return this->rpcSend(msg);
}  // ogmios::Client::nextTransaction

auto ogmios::Client::hasTransaction(std::string_view txid) -> std::future<json>
{
    auto msg = json{
        {"method", "hasTransaction"},
        {"params", {{"id", txid}}},
    };
    return this->rpcSend(msg);
}  // ogmios::Client::hasTransaction

////////////////////////////////////////////////////////////////////////////////
// Transaction Submission Mini-Protocol
////////////////////////////////////////////////////////////////////////////////

auto ogmios::Client::submitTransaction(const json& params) -> std::future<json>
{
    auto msg = json{
        {"method", "submitTransaction"},
        {"params", params},
    };
    return this->rpcSend(msg);
}  // ogmios::Client::submitTransaction

auto ogmios::Client::submitTransactionCbor(const std::string& cborhex)
    -> std::future<json>
{
    auto params = json{{"transaction", {{"cbor", cborhex}}}};
    return this->submitTransaction(params);
}  // ogmios::Client::submitTransactionCbor

auto ogmios::Client::evaluateTransaction(const json& params)
    -> std::future<json>
{
    auto msg = json{
        {"method", "evaluateTransaction"},
        {"params", params},
    };
    return this->rpcSend(msg);
}  // ogmios::Client::evaluateTransaction

auto ogmios::Client::evaluateTransactionCbor(std::string_view cborhex)
    -> std::future<json>
{
    auto params = json{{"transaction", {{"cbor", cborhex}}}};
    return this->evaluateTransaction(params);
}  // ogmios::Client::evaluateTransactionCbor
