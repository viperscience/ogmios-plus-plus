# Example: Get Mempool Contents
This example project uses __libogmios__ to get the contents of the mempool and waits for it to be empty.
Transaction building and signing is enabled via __libcardano__ functionality.

## Build
Build the example as part of the top-level libogmios library build using the `BUILD_LIBOGMIOS_EXAMPLES` flag.
A full example is shown below. 
Note that the project relies on both submodules and vcpkg for dependencies.

    # Clone the project (with submodules!)
    git clone --recurse-submodules https://gitlab.com/viperscience/libogmios.git
    cd libogmios

    # Run the vcpkg bootstrap scripts (first install only)
    ./vcpkg/bootstrap-vcpkg.sh

    # Setup the Cmake project
    # Note that vcpkg will be used by default so there is no need to specify here.
    # Set the flag to build the examples.
    cmake -B build/ -S . -DBUILD_LIBOGMIOS_EXAMPLES=ON
    cmake --build build/ --parallel 4

## Run
Run the example as shown (replace `build` with your build directory if differently named).

    ./build/bin/getmempoolcontents

## Output

![Example ](/docs/img/example_output_get_mempool_contents.png)
