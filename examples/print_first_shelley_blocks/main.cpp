// Copyright (c) 2024 Viper Science LLC
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include <stdexcept>
#include <string_view>

// Third-party libraries
#include <nlohmann/json.hpp>
#include <ogmios/client.hpp>
#include <spdlog/spdlog.h>

using json = nlohmann::json;
using namespace nlohmann::literals;
using namespace std::literals;

namespace
{
constexpr auto NETWORK = "preprod"sv;
}

auto main() -> int
{
    spdlog::set_level(spdlog::level::debug);  // Set global log level

    // Create a client to manage the Ogmios server connection.
    auto client = ogmios::Client("ws://localhost:1337");

    // Setup the logging callback to print information to stdout.
    client.setLoggerCallback(
        [](int level, const std::string& msg)
        { spdlog::log(static_cast<spdlog::level::level_enum>(level), msg); }
    );

    // Create a point from the last block of the Byron era

    auto point = "[]"_json;
    if (NETWORK == "mainnet"sv)
    {
        point[0]["slot"] = 4492799;
        point[0]["id"] =
            "f8084c61b6a238acec985b59310b6ecec49c0ab8352249afd7268da5cff2a457";
    }
    else if (NETWORK == "preprod"sv)
    {
        point[0]["slot"] = 84242;
        point[0]["id"] =
            "45899e8002b27df291e09188bfe3aeb5397ac03546a7d0ead93aa2500860f1af";
    }
    else
    {
        throw std::runtime_error("Unsupported network");
    }

    // Get the intersection of the last Byron block
    // The findIntersection method runs asynchronously and returns a future,
    // calling get here will wait indefinitely for the result (use wait_for in
    // production code).
    client.findIntersection(point).get();

    // Now print the next 3 blocks, which will be in the Shelley era
    for (auto blocksPrinted = 0; blocksPrinted < 3;)
    {
        auto result = client.nextBlock().get();
        if (result["result"]["direction"] == "forward")
        {
            spdlog::info(
                "Shelley Block #{}: Height = {}, ID = {}",
                ++blocksPrinted,
                result["result"]["block"]["height"].dump(),
                result["result"]["block"]["id"].dump()
            );
        }
    }

    return 0;
}  // main
