// Copyright (c) 2024 Viper Science LLC
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include <signal.h>

// Third-party libraries
#include <nlohmann/json.hpp>
#include <ogmios/client.hpp>
#include <spdlog/spdlog.h>

using json = nlohmann::json;
using namespace nlohmann::literals;

namespace
{
volatile bool global_exit = false;
}

auto shutdown_handler(int s) -> void
{
    spdlog::info("Program received signal {}", s);
    global_exit = true;
}  // shutdown_handler

auto main() -> int
{
    signal(SIGINT, shutdown_handler);  // Stop the program with CTRL-C

    spdlog::set_level(spdlog::level::debug);  // Set global log level

    // Create a client to manage the Ogmios server connection.
    auto client = ogmios::Client("ws://localhost:1337");

    // Setup the logging callback to print information to stdout.
    client.setLoggerCallback(
        [](int level, const std::string& msg)
        { spdlog::log(static_cast<spdlog::level::level_enum>(level), msg); }
    );

    // The findIntersection method runs asynchronously and returns a future,
    // calling get here will wait indefinitely for the result (use wait_for in
    // production code).
    auto result = client.findIntersectionOrigin().get();

    // Create a point from the current tip.
    auto tip = "[]"_json;
    tip[0]["slot"] = result["result"]["tip"]["slot"];
    tip[0]["id"] = result["result"]["tip"]["id"];

    // Start streaming blocks from the current tip. The logger will print the
    // messages to stdout.
    client.findIntersection(tip).get();
    while (!global_exit)
    {
        client.nextBlock().get();
    }

    return 0;
}  // main
