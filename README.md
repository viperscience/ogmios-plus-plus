# libogmios

[![Pipeline](https://gitlab.com/viperscience/libogmios/badges/main/pipeline.svg)](https://gitlab.com/viperscience/libogmios/-/pipelines)
[![Codecov](https://codecov.io/gl/viperscience/libogmios/graph/badge.svg?token=KJB4PM1GVS)](https://codecov.io/gl/viperscience/libogmios)
[![Catalyst](https://img.shields.io/badge/catalyst-fund10-violet)](https://cardano.ideascale.com/c/idea/105214)

-----

[Ogmios](https://ogmios.dev/) is a lightweight bridge inteface for cardano-node. It offers a WebSockets API that enables local clients to speak Ouroboros' mini-protocols via JSON/RPC.
**libogmios** in an Ogmios client written in _modern_ C++ designed for ease of use.

## Building from Source
A cmake build setup is included with the project source. Dependencies are mangaged via vcpkg, which is included as a submodule. Including vcpgk as a submodule is prefered because it fixes the dependency versions. Use the following steps as a guide for building from source.

    # Clone the source (with submodules!)
    git clone --recurse-submodules https://gitlab.com/viperscience/libogmios.git
    cd libogmios

    # Run the vcpkg bootstrap scripts (first install only)
    ./vcpkg/bootstrap-vcpkg.sh

    # Setup the Cmake project
    # Note that vcpkg will be used by default so there is no need to specify here.
    cmake -B cmake-build-release -S .
    cmake --build cmake-build-release/ --parallel 4

    # Optionally run tests post build
    ctest --test-dir cmake-build-release/ --output-on-failure --parallel 4 -T Test

    # Install the library
    cmake --install cmake-build-release/

A Docker build example is also available using the supplied Dockerfile.

    docker build -t libogmios:latest .

### Building Examples

The project contains several example programs that may be compiled and executed.
To include the examples during the build include the `BUILD_LIBOGMIOS_EXAMPLES` flag.
An example is shown below.

    cmake -B build/ -S . -DBUILD_LIBOGMIOS_EXAMPLES=ON
    cmake --build build/ --parallel 4

Note that some of the examples require [libcardano](https://gitlab.com/viperscience/libcardano) 
(included as a submodule), which requires GCC 12+.
The examples will be located inside the `bin` directory inside the build directory.

## Support
Viper Science Email or Discord.

## Contributing
Collaboration is welcome. Please ensure new code is formatted with clang-format.

## Authors and Acknowledgments

### Open-Source Software
Libogmios relies on other excellent open-source software.

* [Catch2](https://github.com/catchorg/Catch2): A modern, C++-native, test framework for unit-tests.
* [IXWebSocket](https://github.com/machinezone/IXWebSocket): IXWebSocket is a C++ library for WebSocket client and server development.
* [nlohmann-json](https://github.com/nlohmann/json): JSON for Modern C++.
* [spdlog](https://github.com/gabime/spdlog): Very fast, header-only/compiled, C++ logging library.
* [stduuid](https://github.com/mariusbancila/stduuid): A C++17 cross-platform implementation for UUIDs.

The provided Dockerfile demonstrates how to install the required dependencies and build the library.
The Cmake build system included in the repository uses the popular [vcpgk](https://vcpkg.io/en/index.html) to manage the dependencies.

## License
This project is fully open source and licensed under the Apache License Version 2.0.
