FROM python:3.11-slim-bookworm

RUN apt-get update && apt-get install -y \
  build-essential \
  curl \
  git \
  libssl-dev \
  libtool \
  pkg-config \
  zip \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

# Install the newer version of CMake.
WORKDIR /tmp
RUN curl -LO https://github.com/Kitware/CMake/releases/download/v3.28.3/cmake-3.28.3-linux-x86_64.tar.gz \
  && tar --extract --file cmake-3.28.3-linux-x86_64.tar.gz \
  && mv cmake-3.28.3-linux-x86_64/bin/* /usr/local/bin \
  && mv cmake-3.28.3-linux-x86_64/share/cmake-3.28 /usr/local/share/ \
  && rm -rf cmake*

# Build and install Botan 3.
# Required for the examples using libcardano.
WORKDIR /tmp
RUN curl -LO https://botan.randombit.net/releases/Botan-3.1.1.tar.xz \
  && tar --extract --file Botan-3.1.1.tar.xz \
  && cd Botan-3.1.1 \
  && python3 ./configure.py \
  && make -j8 \
  && make install \
  && cd .. && rm -rf Botan*

# Install the Cardano fork of libsodium.
# Required for the examples using libcardano.
WORKDIR /tmp
RUN git clone https://github.com/input-output-hk/libsodium \
  && cd libsodium \
  && git checkout dbb48cc \
  && ./autogen.sh \
  && ./configure \
  && make -j8 \
  && make install \
  && cd .. && rm -rf libsodium
RUN export LD_LIBRARY_PATH="/usr/local/lib:$LD_LIBRARY_PATH" \
  && export PKG_CONFIG_PATH="/usr/local/lib/pkgconfig:$PKG_CONFIG_PATH"

# Copy the entire libogmios project source.
# Ensure the submodules are included and vcpkg as downloaded all dependencies.
COPY . /opt
WORKDIR /opt

# Debug build
# Note: We cannot run tests here without an ogmios server running.
RUN cmake -S . -B cmake-build-debug/ -D CMAKE_BUILD_TYPE=Debug \
  && cmake --build cmake-build-debug/ --parallel 8
# && ctest --test-dir cmake-build-debug/ --output-on-failure -T Test -T Coverage

# Release build and install.
# Note: We cannot run tests here without an ogmios server running.
RUN cmake -S . -B cmake-build-release/ -D CMAKE_BUILD_TYPE=Release \
  && cmake --build cmake-build-release/ --parallel 8 \
  && cmake --install cmake-build-release/

# Build the examples

WORKDIR /opt/examples/async_queries
RUN cmake -B ./build -S . \
  &&  cmake --build ./build --parallel 8

WORKDIR /opt/examples/build_tx_libcardano
RUN cmake -B ./build -S . \
  &&  cmake --build ./build --parallel 8

WORKDIR /opt/examples/find_wallet_transactions
RUN cmake -B ./build -S . \
  &&  cmake --build ./build --parallel 8

WORKDIR /opt/examples/live_block_viewer
RUN cmake -B ./build -S . \
  &&  cmake --build ./build --parallel 8

WORKDIR /opt/examples/print_first_shelley_blocks
RUN cmake -B ./build -S . \
  &&  cmake --build ./build --parallel 8
