# libogmios Testing

The test in this section are primarily integration tests that verify the proper request/reponse interaction with a running instance of Ogmios.

## Prerequisites
In order for all tests to pass, Ogmios must be connected to a fully-synced cardano-node on the Pre-prod testnet.

1. Ogmios started and available at ws://localhost:1337
2. The Cardano-node is fully sync'ed
3. Pre-prod testnet
4. Ogmios is not using the `--strinct-rpc` flag