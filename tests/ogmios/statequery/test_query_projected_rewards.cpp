// Copyright (c) 2024 Viper Science LLC
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include <catch2/catch_test_macros.hpp>
#include <chrono>
#include <nlohmann/json.hpp>

#include "../test_fixtures.hpp"

using json = nlohmann::json;

using namespace std::chrono_literals;

TEST_CASE_METHOD(OgmiosClientTestFixture, "test_QueryProjectedRewards")
{
    if (!client.has_value())
    {
        SKIP("Ogmios server not running. Skipping test.");
    }

    SECTION("vectors")
    {
        auto stakes = std::vector<size_t>();
        stakes.push_back(1000000);

        auto keys = std::vector<std::string>();
        keys.push_back(
            "stake_test1urlvxquq2pm8teqed7ek4sgjuc0zrx07z9czmy5fvwey92cq8c5dh"
        );

        auto scripts = std::vector<std::string>();
        scripts.push_back(
            "stake_test1urlvxquq2pm8teqed7ek4sgjuc0zrx07z9czmy5fvwey92cq8c5dh"
        );

        auto future_result =
            client->queryProjectedRewards(stakes, keys, scripts);
        REQUIRE(future_result.wait_for(5s) == std::future_status::ready);

        auto result = future_result.get();
        for (auto &[key, val] : result["result"]["1000000"].items())
        {
            REQUIRE(key.substr(0, 4) == "pool");
            REQUIRE(val.contains("ada"));
        }
        for (auto &[key, val] : result["result"][scripts[0]].items())
        {
            REQUIRE(key.substr(0, 4) == "pool");
            REQUIRE(val.contains("ada"));
        }
    }

    SECTION("json")
    {
        const auto params = R"({
            "stake": [
                {
                    "ada": {
                        "lovelace": 1000000
                    }
                }
            ],
            "keys": [
                "stake_test1urlvxquq2pm8teqed7ek4sgjuc0zrx07z9czmy5fvwey92cq8c5dh"
            ],
            "scripts": [
                "stake_test1urlvxquq2pm8teqed7ek4sgjuc0zrx07z9czmy5fvwey92cq8c5dh"
            ]
        })"_json;

        auto future_result = client->queryProjectedRewards(params);
        REQUIRE(future_result.wait_for(5s) == std::future_status::ready);

        auto result = future_result.get();
        for (auto &reward : result["result"])
        {
            for (auto &[key, val] : reward.items())
            {
                REQUIRE(key.substr(0, 4) == "pool");
                REQUIRE(val.contains("ada"));
            }
        }
    }

    SECTION("some")
    {
        auto stakes = std::vector<size_t>();
        stakes.push_back(1000000);

        auto keys = std::vector<std::string>();

        auto scripts = std::vector<std::string>();
        scripts.push_back(
            "stake_test1urlvxquq2pm8teqed7ek4sgjuc0zrx07z9czmy5fvwey92cq8c5dh"
        );

        auto future_result =
            client->queryProjectedRewards(stakes, keys, scripts);
        REQUIRE(future_result.wait_for(5s) == std::future_status::ready);

        auto result = future_result.get();
        for (auto &[key, val] : result["result"]["1000000"].items())
        {
            REQUIRE(key.substr(0, 4) == "pool");
            REQUIRE(val.contains("ada"));
        }
        for (auto &[key, val] : result["result"][scripts[0]].items())
        {
            REQUIRE(key.substr(0, 4) == "pool");
            REQUIRE(val.contains("ada"));
        }
    }

    SECTION("none")
    {
        auto stakes = std::vector<size_t>();
        auto keys = std::vector<std::string>();
        auto scripts = std::vector<std::string>();

        CHECK_THROWS(client->queryProjectedRewards(stakes, keys, scripts));
    }
}
