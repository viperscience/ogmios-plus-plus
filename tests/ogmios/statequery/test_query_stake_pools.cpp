// Copyright (c) 2024 Viper Science LLC
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include <catch2/catch_test_macros.hpp>
#include <chrono>

#include "../test_fixtures.hpp"

using namespace std::chrono_literals;

TEST_CASE_METHOD(OgmiosClientTestFixture, "test_QueryStakePools")
{
    if (!client.has_value())
    {
        SKIP("Ogmios server not running. Skipping test.");
    }

    SECTION("all")
    {
        auto future_result = client->queryStakePools();
        REQUIRE(future_result.wait_for(2s) == std::future_status::ready);

        auto result = future_result.get();
        for (auto& [key, val] : result["result"].items())
        {
            REQUIRE(key.substr(0, 4) == "pool");
            REQUIRE(val.contains("vrfVerificationKeyHash"));
        }
    }

    SECTION("some")
    {
        constexpr auto pool_count = 10;
        auto pool_ids = std::vector<std::string>();

        auto future_result = client->queryLiveStakeDistribution();
        REQUIRE(future_result.wait_for(2s) == std::future_status::ready);

        auto result = future_result.get();
        for (auto& [key, val] : result["result"].items())
        {
            pool_ids.push_back(key);
            if (pool_ids.size() == pool_count)
            {
                break;
            }
        }

        auto future_result2 = client->queryStakePools(pool_ids);
        REQUIRE(future_result2.wait_for(2s) == std::future_status::ready);

        auto result2 = future_result2.get();
        REQUIRE(result2["result"].size() == pool_count);
        for (auto& [key, val] : result2["result"].items())
        {
            REQUIRE(key.substr(0, 4) == "pool");
            REQUIRE(val.contains("vrfVerificationKeyHash"));
        }
    }
}