// Copyright (c) 2024 Viper Science LLC
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include <catch2/catch_test_macros.hpp>
#include <chrono>
#include <nlohmann/json.hpp>

#include "../test_fixtures.hpp"

using json = nlohmann::json;

using namespace std::chrono_literals;

TEST_CASE_METHOD(OgmiosClientTestFixture, "test_QueryUtxo")
{
    if (!client.has_value())
    {
        SKIP("Ogmios server not running. Skipping test.");
    }

    SECTION("address")
    {
        const auto params = R"(
            {
                "addresses": [
                    "addr_test1vz09v9yfxguvlp0zsnrpa3tdtm7el8xufp3m5lsm7qxzclgmzkket"
                ]
            }
        )"_json;

        auto future_result = client->queryUtxo(params);
        REQUIRE(future_result.wait_for(10s) == std::future_status::ready);

        auto result = future_result.get();
        REQUIRE(!result["result"].empty());
    }

    SECTION("address_empty")
    {
        const auto params = R"(
            {
                "addresses": [
                    "addr1q9d34spgg2kdy47n82e7x9pdd6vql6d2engxmpj20jmhuc2047yqd4xnh7u6u5jp4t0q3fkxzckph4tgnzvamlu7k5psuahzcp"
                ]
            }
        )"_json;

        auto future_result = client->queryUtxo(params);
        REQUIRE(future_result.wait_for(10s) == std::future_status::ready);

        auto result = future_result.get();
        REQUIRE(result["result"].empty());
    }

    SECTION("output_ref")
    {
        const auto params = R"(
            {
                "addresses": [
                    "addr_test1qznwxwlxrq9p58c4mfmxdh45zpmu5wfhflwhp69cq7v667h7cvpcq5rkwhjpjmandtq39es7yxvluyts9kfgjcajg24sx9yfqe"
                ]
            }
        )"_json;

        auto future_result = client->queryUtxo(params);
        REQUIRE(future_result.wait_for(30s) == std::future_status::ready);

        auto result = future_result.get();
        if (!result["result"].empty())
        {
            auto params2 = json({{"outputReferences", {result["result"][0]}}});
            params2["outputReferences"][0].erase("address");
            params2["outputReferences"][0].erase("value");

            auto future_result1 = client->queryUtxo(params2);
            if (future_result1.wait_for(20s) != std::future_status::ready)
            {
                throw std::runtime_error("queryUtxo timed out");
            }

            auto result1 = future_result1.get();
            REQUIRE(result["result"][0] == result1["result"][0]);
        }
    }

    // SECTION("all")
    // {
    //     auto future_result = client->queryUtxo();
    //     REQUIRE(future_result.wait_for(200s) == std::future_status::ready);
    //
    //     auto result = future_result.get();
    //     REQUIRE(result["result"] == "");
    // }
}