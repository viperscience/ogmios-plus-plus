// Copyright (c) 2024 Viper Science LLC
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include <catch2/catch_test_macros.hpp>
#include <chrono>

#include "../test_fixtures.hpp"

using namespace std::chrono_literals;

TEST_CASE_METHOD(OgmiosClientTestFixture, "test_QueryGenesisConfiguration")
{
    if (!client.has_value())
    {
        SKIP("Ogmios server not running. Skipping test.");
    }

    SECTION("byron")
    {
        auto future_result = client->queryGenesisConfiguration("byron");
        REQUIRE(future_result.wait_for(2s) == std::future_status::ready);

        auto result = future_result.get();
        REQUIRE(result["result"]["era"] == "byron");
        REQUIRE(result["result"]["genesisKeyHashes"].size() == 7);
        REQUIRE(
            result["result"]["updatableParameters"]["slotDuration"] == 20000
        );
    }

    SECTION("shelley")
    {
        auto future_result = client->queryGenesisConfiguration("shelley");
        REQUIRE(future_result.wait_for(2s) == std::future_status::ready);

        auto result = future_result.get();
        REQUIRE(result["result"]["era"] == "shelley");
    }

    SECTION("alonzo")
    {
        auto future_result = client->queryGenesisConfiguration("alonzo");
        REQUIRE(future_result.wait_for(2s) == std::future_status::ready);

        auto result = future_result.get();
        REQUIRE(result["result"]["era"] == "alonzo");
    }

    SECTION("badEra")
    {
        REQUIRE_THROWS(client->queryGenesisConfiguration("babbage"));
    }
}