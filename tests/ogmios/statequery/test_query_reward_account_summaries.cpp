// Copyright (c) 2024 Viper Science LLC
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include <catch2/catch_test_macros.hpp>
#include <chrono>
#include <nlohmann/json.hpp>

#include "../test_fixtures.hpp"

using json = nlohmann::json;

using namespace std::chrono_literals;

TEST_CASE_METHOD(OgmiosClientTestFixture, "test_QueryRewardAccountSummaries")
{
    if (!client.has_value())
    {
        SKIP("Ogmios server not running. Skipping test.");
    }

    SECTION("key")
    {
        const auto params = R"(
            {
                "keys": [
                    "stake_test1uq9947tw3wg5y7l0wmc59eyuz2fc4p76m6qjnwmw5ss58vsmzhs0n"
                ]
            }
        )"_json;

        auto future_result = client->queryRewardAccountSummaries(params);
        REQUIRE(future_result.wait_for(2s) == std::future_status::ready);

        auto result = future_result.get();
        for (const auto& [key, value] : result["result"].items())
        {
            REQUIRE(value.contains("delegate"));
            REQUIRE(value.contains("rewards"));
        }
    }

    SECTION("empty key")
    {
        const auto params = R"(
            {
                "keys": [
                    "stake_test1urlvxquq2pm8teqed7ek4sgjuc0zrx07z9czmy5fvwey92cq8c5dh"
                ]
            }
        )"_json;

        auto future_result = client->queryRewardAccountSummaries(params);
        REQUIRE(future_result.wait_for(2s) == std::future_status::ready);

        auto result = future_result.get();
        REQUIRE(result["result"] == "{}"_json);
    }

    SECTION("empty script")
    {
        const auto params = R"(
            {
                "scripts": [
                    "stake_test1urlvxquq2pm8teqed7ek4sgjuc0zrx07z9czmy5fvwey92cq8c5dh"
                ]
            }
        )"_json;

        auto future_result = client->queryRewardAccountSummaries(params);
        REQUIRE(future_result.wait_for(2s) == std::future_status::ready);

        auto result = future_result.get();
        REQUIRE(result["result"] == "{}"_json);
    }
}