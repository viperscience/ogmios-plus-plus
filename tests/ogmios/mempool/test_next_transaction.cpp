// Copyright (c) 2024 Viper Science LLC
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include <catch2/catch_test_macros.hpp>
#include <chrono>

#include "../test_fixtures.hpp"

using namespace std::chrono_literals;

TEST_CASE_METHOD(OgmiosClientTestFixture, "test_NextTransaction")
{
    if (!client.has_value())
    {
        SKIP("Ogmios server not running. Skipping test.");
    }

    SECTION("one")
    {
        // Try getting the next transaction before acquiring the mempool
        auto future_error_result = client->nextTransaction();
        REQUIRE(future_error_result.wait_for(2s) == std::future_status::ready);

        auto error_result = future_error_result.get();
        REQUIRE(error_result["jsonrpc"] == "2.0");
        REQUIRE(error_result["method"] == "nextTransaction");
        REQUIRE(error_result.contains("error"));
        REQUIRE(error_result["error"]["code"] == 4000);

        // Acquire a mempool snapshot
        REQUIRE(
            client->acquireMempool().wait_for(2s) == std::future_status::ready
        );

        auto future_result = client->nextTransaction();
        REQUIRE(future_result.wait_for(2s) == std::future_status::ready);

        auto result = future_result.get();
        REQUIRE(result["jsonrpc"] == "2.0");
        REQUIRE(result["method"] == "nextTransaction");
        REQUIRE(!result.contains("error"));
        REQUIRE(result.contains("result"));
    }

    SECTION("all")
    {
        // Try getting the next transaction before acquiring the mempool
        auto future_error_result = client->nextTransaction();
        REQUIRE(future_error_result.wait_for(2s) == std::future_status::ready);

        auto error_result = future_error_result.get();
        REQUIRE(error_result["jsonrpc"] == "2.0");
        REQUIRE(error_result["method"] == "nextTransaction");
        REQUIRE(error_result.contains("error"));
        REQUIRE(error_result["error"]["code"] == 4000);

        // Acquire a mempool snapshot
        REQUIRE(
            client->acquireMempool().wait_for(2s) == std::future_status::ready
        );

        auto future_result = client->nextTransaction(true);
        REQUIRE(future_result.wait_for(2s) == std::future_status::ready);

        auto result = future_result.get();
        REQUIRE(result["jsonrpc"] == "2.0");
        REQUIRE(result["method"] == "nextTransaction");
        REQUIRE(!result.contains("error"));
        REQUIRE(result.contains("result"));
    }
}
