// Copyright (c) 2024 Viper Science LLC
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include <catch2/catch_test_macros.hpp>
#include <chrono>

#include "../test_fixtures.hpp"

using namespace std::chrono_literals;

TEST_CASE_METHOD(OgmiosClientTestFixture, "test_ReleaseMempool")
{
    if (!client.has_value())
    {
        SKIP("Ogmios server not running. Skipping test.");
    }

    // Try releasing a mempool snapshot before acquiring one
    auto future_result = client->releaseMempool();
    REQUIRE(future_result.wait_for(2s) == std::future_status::ready);

    auto result = future_result.get();
    REQUIRE(result["jsonrpc"] == "2.0");
    REQUIRE(result["method"] == "releaseMempool");
    REQUIRE(result.contains("error"));
    REQUIRE(result["error"]["code"] == 4000);

    // Acquire a mempool snapshot
    REQUIRE(client->acquireMempool().wait_for(2s) == std::future_status::ready);

    auto future_result2 = client->releaseMempool();
    REQUIRE(future_result2.wait_for(2s) == std::future_status::ready);

    auto result2 = future_result2.get();
    REQUIRE(result2["jsonrpc"] == "2.0");
    REQUIRE(result2["method"] == "releaseMempool");
    REQUIRE(!result2.contains("error"));
    REQUIRE(result2.contains("result"));
    REQUIRE(result2["result"]["released"] == "mempool");
}
