// Copyright (c) 2024 Viper Science LLC
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include <catch2/catch_test_macros.hpp>
#include <chrono>

#include "../test_fixtures.hpp"

using namespace std::chrono_literals;

//
// The following test cases are specific to the pre-prod network.
//

TEST_CASE_METHOD(OgmiosClientTestFixture, "test_NextBlock")
{
    if (!client.has_value())
    {
        SKIP("Ogmios server not running. Skipping test.");
    }

    SECTION("origin")
    {
        auto origin_future = client->findIntersectionOrigin();
        REQUIRE(origin_future.wait_for(2s) == std::future_status::ready);
        auto origin_intersection = origin_future.get();
        REQUIRE(origin_intersection["result"]["intersection"] == "origin");

        auto backwards_future = client->nextBlock();
        REQUIRE(backwards_future.wait_for(2s) == std::future_status::ready);
        auto backwards_result = backwards_future.get();
        REQUIRE(backwards_result["result"]["direction"] == "backward");
        REQUIRE(backwards_result["result"]["tip"]["height"] > 0);
        REQUIRE(backwards_result["result"]["tip"]["slot"] > 0);

        auto block0_future = client->nextBlock();
        REQUIRE(block0_future.wait_for(2s) == std::future_status::ready);
        auto block0_result = block0_future.get();
        REQUIRE(block0_result["result"]["direction"] == "forward");
        REQUIRE(block0_result["result"]["block"]["height"] == 0);
        REQUIRE(block0_result["result"]["block"]["era"] == "byron");

        auto block1_future = client->nextBlock();
        REQUIRE(block1_future.wait_for(2s) == std::future_status::ready);
        auto block1_result = block1_future.get();
        REQUIRE(block1_result["result"]["direction"] == "forward");
        REQUIRE(block1_result["result"]["block"]["height"] == 1);
        REQUIRE(block1_result["result"]["block"]["era"] == "byron");
    }

    SECTION("next")
    {
        auto point = "[]"_json;
        point[0]["slot"] = 53157488;
        point[0]["id"] =
            "f398182819ae27604cf73602cad6cb4a296c398e12a33e81b5f9b5161b3c46df";

        REQUIRE(
            client->findIntersection(point).wait_for(2s) ==
            std::future_status::ready
        );

        auto backwards_future = client->nextBlock();
        REQUIRE(backwards_future.wait_for(2s) == std::future_status::ready);
        auto backwards_result = backwards_future.get();
        REQUIRE(backwards_result["result"]["direction"] == "backward");
        REQUIRE(backwards_result["result"]["tip"]["height"] > 0);
        REQUIRE(backwards_result["result"]["tip"]["slot"] > 0);

        auto block_future = client->nextBlock();
        REQUIRE(block_future.wait_for(2s) == std::future_status::ready);
        auto block_result = block_future.get();
        REQUIRE(block_result["result"]["direction"] == "forward");
        REQUIRE(
            block_result["result"]["block"]["ancestor"] ==
            "f398182819ae27604cf73602cad6cb4a296c398e12a33e81b5f9b5161b3c46df"
        );
        REQUIRE(block_result["result"]["block"]["height"] == 1970263);
        REQUIRE(block_result["result"]["block"]["era"] == "babbage");
    }
}