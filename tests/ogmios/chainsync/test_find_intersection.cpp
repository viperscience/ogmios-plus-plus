// Copyright (c) 2024 Viper Science LLC
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include <catch2/catch_test_macros.hpp>
#include <chrono>

#include "../test_fixtures.hpp"

using namespace std::chrono_literals;

TEST_CASE_METHOD(OgmiosClientTestFixture, "test_FindIntersection")
{
    if (!client.has_value())
    {
        SKIP("Ogmios server not running. Skipping test.");
    }

    SECTION("origin")
    {
        auto future_result = client->findIntersectionOrigin();
        REQUIRE(future_result.wait_for(2s) == std::future_status::ready);

        auto result = future_result.get();
        REQUIRE(result["jsonrpc"] == "2.0");
        REQUIRE(result["method"] == "findIntersection");
        REQUIRE(result["result"]["intersection"] == "origin");
        REQUIRE(result["result"]["tip"]["slot"] > 0);
    }

    SECTION("last_byron_block")
    {
        auto point = "[]"_json;
        point[0]["slot"] = 84242;
        point[0]["id"] =
            "45899e8002b27df291e09188bfe3aeb5397ac03546a7d0ead93aa2500860f1af";

        auto future_result = client->findIntersection(point);
        REQUIRE(future_result.wait_for(2s) == std::future_status::ready);

        auto result = future_result.get();
        REQUIRE(result["result"]["intersection"] == point[0]);
    }

    SECTION("tip")
    {
        auto origin_future = client->findIntersectionOrigin();
        REQUIRE(origin_future.wait_for(2s) == std::future_status::ready);

        auto origin = origin_future.get();
        auto point = "[]"_json;
        point[0]["slot"] = origin["result"]["tip"]["slot"];
        point[0]["id"] = origin["result"]["tip"]["id"];

        auto intersection_future = client->findIntersection(point);
        REQUIRE(intersection_future.wait_for(2s) == std::future_status::ready);

        auto intersection = intersection_future.get();
        REQUIRE(intersection["result"]["intersection"] == point[0]);
    }

    SECTION("invalid_block")
    {
        auto point = "[]"_json;
        point[0]["slot"] = 1;
        point[0]["id"] =
            "1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcdef";

        auto intersection_future = client->findIntersection(point);
        REQUIRE(intersection_future.wait_for(2s) == std::future_status::ready);

        auto intersection = intersection_future.get();
        REQUIRE(intersection["error"]["code"] == 1000);
        REQUIRE(intersection["error"]["message"] == "No intersection found.");
    }
}